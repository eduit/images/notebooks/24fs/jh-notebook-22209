FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-base-ethz:3.0.0-16

USER root

RUN pip3 uninstall  -y traitlets
RUN mamba install -q -c defaults -c conda-forge -c bioconda -c bokeh \
  scikit-learn \
  matplotlib \
  seaborn \
  biopython \
  nbgitpuller \
  python-ternary \
  && \
  mamba clean --all

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install -U --proxy=http://proxy.ethz.ch:3128 --default-timeout=100 \
  jax>=0.4.21 \
  jaxlib>=0.4.21 \
  jupyterlab-git>=0.50.0 \
  plotly>=5.18.0 \
  sympy>=1.12.0

USER jovyan
